#!/bin/bash
# Usage:
# ./mount.sh <path/to/loopback/file>
#
# Script that authomatizes the proces of mount a file that is a loopback device created with losetup. 
# Also if is an encrypted volume
# Script workflow
# 1- Get the file and mount it on the first /dev/loopX volume.
# 2- Check if the volume is encrypted and luksOpen it
# 3- Mount it.

echo "[X] Starting script as $(whoami)..."

filename=$(basename $1) 	# get the filename
dev=$(losetup -f) 			# first free device

if [ ! -f $1 ]; then
	echo "[X] ERROR: File not found!"
	exit 
elif mount | grep $filename > /dev/null; then

	echo "[X] ERROR File already mounted"
	echo "$(mount | grep $filename)"
elif [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root" 
	exec sudo -- "$0" "$@"
fi

echo "[X] Losetup mounting $1 on $dev"
losetup $dev $1 
if [[ $? != 0 ]]; then
	echo "[X] ERROR mounting losetup"
	exit
fi

luksDev=$(sudo cryptsetup luksDump $1)
if [[ $? != 0 ]]; then
	echo "[X] Can't open Luks or command failed"
elif [[ $luksDev ]]; then
	echo "Luks find"
	echo "[X] Crypsetup opening $dev on $filename"
	if [ -f "/dev/mapper/$filename" ]; then
		echo "[!] Umounting $filename before the operation"
		cryptsetup luksClose $filename
	fi
	
    cryptsetup luksOpen $dev $filename
    if [[ $? != 0 ]]; then
		echo "[X] ERROR on luksOpen"
		exit
	fi
    dev="/dev/mapper/$filename"
fi

mountpoint="/media/$(whoami)/$filename"

echo "[X] Mounting $dev on $mountpoint"

if [ ! -d $mountpoint ]; then
  mkdir -p $mountpoint;
fi

mount $dev $mountpoint
if [[ $? != 0 ]]; then
	echo "[X] ERROR mounting"
	exit
fi

echo "[X] Mounted."
