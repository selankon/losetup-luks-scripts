#!/bin/bash
# Usage:
# ./create.sh <path/to/new/file>
#
# Script that creates a loopback volume, and encrypt it if is wanted.

FILENAMES=()

formating() {
	mkfs.ext4 $1
	if [[ $? != 0 ]]; then
		echo "[X] ERROR formating $1"
		return 1
	fi
}

args=( $@ )
while [[ $# -gt 0 ]]
	do
	key="$1"

	case $key in
		-h|--help)
		echo "Script that creates a loopback volume, and encrypt it if is wanted."
		echo "		Usage:"
		echo "		./create.sh [options] [file|files]"
		echo "		-e|--encrypt	Encrypt the volume using Luks"
		exit
		;;
		-e|--encrypt)
		ENCRYPT="true"
		shift # past argument
		;;
		*)
		FILENAMES+=("$1") # save it in an array for later
		shift 
		;;
	esac
done

fileLenght=${#FILENAMES[@]}
if [[ $fileLenght -eq 0 ]]; then
	echo "[X] ERROR: you mast to provide at least one filename"
	exit
fi

echo "[X] Starting script as $(whoami)..."

if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	set -- "${args[@]}"
	echo "BEFORE ROOT $@"
	exec sudo -- "$0" "$@"
fi

if [[ $ENCRYPT = "true" ]] ; then
	echo "[X] SET ENCRYPTION TO $ENCRYPT"

fi
encrypt=$ENCRYPT

set -- "${FILENAMES[@]}" # restore positional parameters

echo "[X] Creating loopback $fileLenght files: ${FILENAMES[*]}"

for var in "${FILENAMES[@]}"; do

	if [ -f ${var} ]; then
		echo "[X] ERROR: ${var} file already exist!"
		continue
	fi
	echo "[X] Creating files: ${var}"
	
	dd if=/dev/urandom of=${var} bs=1M count=10
	if [[ $? != 0 ]]; then
		echo "[X] ERROR creating the file ${var}"
		continue
	fi
	
	
	dev=$(losetup -f) 			# first free device
	echo "[X] Mounting loopback volume ${var} on $dev"
	losetup $dev ${var}
	if [[ $? != 0 ]]; then
		echo "[X] ERROR mounting loopback volume ${var} on $dev"
		continue
	fi
	
	echo "[X] Formating file system. $ENCRYPT"
	
	if [[ $ENCRYPT = "true" ]] ; then
		echo '[X] Creating luks device...'
		mapperName=$(basename $var) 
		cryptsetup luksFormat $dev
		if [[ $? != 0 ]]; then
			echo "[X] ERROR on luksFormat $dev"
			continue
		fi
		if [ -f "/dev/mapper/$mapperName" ]; then
			echo "[!] Umounting $mapperName before the operation"
			cryptsetup luksClose $mapperName
		fi
	
		sudo cryptsetup luksOpen $dev $mapperName
		if [[ $? != 0 ]]; then
			echo "[X] ERROR luksOpen ${dev} on $mapperName"
			continue
		fi
		echo "[X] Encryption success"
		
		formating "/dev/mapper/$mapperName"
		
		echo "[X] Closing luks $mapperName"
		cryptsetup luksClose $mapperName
		
		
	else 
		formating $dev
	fi
	
	echo "[X] Closing loopback volume $dev"
	losetup -d  $dev
	
done	


echo "[X] Done."
