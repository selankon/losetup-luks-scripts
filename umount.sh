#!/bin/bash
# Usage:
# ./umount.sh <path/to/mounted/volume|volumeName>
#
# Script that authomatizes the proces to umount and close a loopback device, also encrypted.
# Script workflow
# 1- Umount, giving the name or the mountpoint.
# 2- If was mounted from a Luks device, luksClose it
# 3- Use losetup and close loopback device

echo "[X] Starting script as $(whoami)..."
	
filename=$(basename $1) 	# get the filename

if ! mount | grep $filename > /dev/null; then
	echo "[X] ERROR File is not mounted $filename"
	exit
elif [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root" 
	exec sudo -- "$0" "$@"
fi


mountpoint=$(mount | grep $filename | awk '{ print $3 }')
dev=$(mount | grep $filename | awk '{ print $1 }')
losetup=$(losetup --list | grep $filename | awk '{ print $1 }')

echo "[X] Umounting $mountpoint"
umount $mountpoint
if [[ $? != 0 ]]; then
	echo "[X] ERROR umounting $mountpoint"
	exit
fi

if dmsetup ls | grep $filename > /dev/null; then
	echo "[X] Closing device $dev"
	echo "Is luks device"
	cryptsetup luksClose $filename
	if [[ $? != 0 ]]; then
		echo "[X] ERROR can't close $filename"
		exit
	fi		
fi 


if [ -n "$losetup" ]; then
	echo "[X] Closing loop device $losetup"
	losetup -d $losetup
	if [[ $? != 0 ]]; then
		echo "[X] ERROR can't close loop device $losetup"
	fi	
fi

echo "[X] Deleting $mountpoint"

rm -R $mountpoint
if [[ $? != 0 ]]; then
	echo "[X] ERROR Can't delete $mountpoint"
fi


echo "[X] Finished."
