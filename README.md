# Losetup luks easy volumes

This script will authomatize the creation|mount|umount of [losetup](https://linux.die.net/man/8/losetup) volume files **encrypted or not** with [LUKS](https://en.wikipedia.org/wiki/Linux_Unified_Key_Setup) file system.

The scripts work as following (as root):

* create.sh : Create a file using dd command and then mount it with losetup and give format to it. With the `-e` option will also encrypt it with luks.

* mount.sh : with absolute or relative path of the file as parameter will mount and|or unencrypt the file. The mountpoint will be `/media/root/<File_Name>`.

* umount.sh : will umount and luksClose (if needed) the given mountpoint. At the end delete the mountpoint path.

Future improvements:

[ ] Add functions as umount all

